import operator
from fdot.fn import Fn


class Placeholder:
    ### Arithmetic Ops ###

    # binary

    def __add__(self, rhs) -> Fn:
        return Fn(operator.add, rhs)

    def __sub__(self, rhs) -> Fn:
        return Fn(operator.sub, rhs)

    def __mul__(self, rhs) -> Fn:
        return Fn(operator.mul, rhs)

    def __truediv__(self, rhs) -> Fn:
        return Fn(operator.truediv, rhs)

    def __floordiv__(self, rhs) -> Fn:
        return Fn(operator.floordiv, rhs)

    # unary

    def __abs__(self):
        return Fn(operator.abs)


    ### Bitwise Ops ###

    # binary

    def __and__(self, other) -> Fn:
        return Fn(operator.and_, other)

    def __or__(self, other) -> Fn:
        return Fn(operator.or_, other)

    # unary

    def not_(self) -> Fn:
        return Fn(operator.not_)

    def __invert__(self) -> Fn:
        return Fn(operator.inv)


    ### Comparison Ops ###

    def is_(self, rhs) -> Fn:
        return Fn(operator.is_, rhs)

    def is_not(self, rhs) -> Fn:
        return Fn(operator.is_not, rhs)

    def __eq__(self, rhs) -> Fn:
        return Fn(operator.eq, rhs)

    def __gt__(self, rhs) -> Fn:
        return Fn(operator.gt, rhs)

    def __ge__(self, rhs) -> Fn:
        return Fn(operator.ge, rhs)

    def __lt__(self, rhs) -> Fn:
        return Fn(operator.lt, rhs)

    def __le__(self, rhs) -> Fn:
        return Fn(operator.le, rhs)

    def __ne__(self, rhs) -> Fn:
        return Fn(operator.ne, rhs)


    ### Truth ###

    def truth(self) -> Fn:
        return Fn(operator.truth)


    def __index__(self) -> Fn:
        return Fn(operator.index)


_ = Placeholder()
