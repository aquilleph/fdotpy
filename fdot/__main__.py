from functools import partial
from pprint import pprint

from .fn import Fn
from .underscore import _


def main():
    ls = [x for x in range(5)]

    ls2 = list(filter(_ < 2, ls))

    pprint(ls)
    pprint(ls2)


if __name__ == '__main__':
    main()
