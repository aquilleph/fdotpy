from functools import partial

identity = lambda x: x


class Fn:
    def __init__(self, fn=identity, *args, **kwargs):
        self.fn = partial(fn, *args, **kwargs)

    def compose(self, f, g):
        return self.__class__(lambda *args, **kwargs: f(g(*args, **kwargs)))

    def __rshift__(self, other):
        return self.compose(other, self.fn)

    def __lshift__(self, other):
        return self.compose(self.fn, other)

    def  __call__(self, *args, **kwargs):
        return self.fn(*args, **kwargs)
